Copy2Pykota
===========

copy2pykota - a collection of bash scripts to use pykota-accounting with web-enabled copiers.

Introduction
------------
At our school we use linuxmuster.net (simply the best open source server for schools) with pykota accounting for the printers.
We have 4 Canon copiers, that used to be accounted differently (using plastic cards).
When we bought a new copier we had to decide, if we buy a rfid-chip-key-system (for old as well as new copiers) or switch to something differen.
There was an offer to use a windows-based accounting system for the copiers only for a minimum of 6000€.
So we decided to choose a different approach:
We use the web-based administration-pages of the copiers to synchronize the users in pykota and to synchronize the copies with virtual printers in pykota.
This meant some detours and compromises (as the possibility to exceed the quota within a day or the need to work with pdf-printers to support real accounting), but after all it works.
We are not sure, if anyone finds it useful, as it is a very special approach to a quite specific situation.
But anyone who is in a similar situation may save some time and use our scripts freely.
In case you create own copier-definitions, please feel encouraged to contribute your files.


Technical details
-----------------
Users are created from an existing csv-file (see the example for structure).
Printer Models are defined in subdirectory drv. These contain the command lines to parse the printers web pages (which are sometimes quite hard to find out).
Printers are defined in subdirectory cfg. They are pointing to printer models als well as virtual printers (within pykota).
The user-script creates all accounts on all active printers.
The accounting-script syncs the copier-accounts with the virtual pykota printers. This is done at nights usually (it is possible more often though).
Everything is usually logged extensively to allow accounting even in case of script- or hardware-failure.
All directories may be altered within the central config file (if you want to redirect log messages for example).


Please feel free to contact us, if you have questions.